Building the project using Maven:
mvn clean package

Launching the project:
java -jar target/test-assignment-money-transfer.jar

Service will be available at http://localhost:8080/rest/money-transfer
