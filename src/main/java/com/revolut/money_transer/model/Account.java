package com.revolut.money_transer.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author Igor
 */
public class Account implements Comparable<Account> {
    private final String code;
    private BigDecimal balance;

    public Account(String code, BigDecimal balance) {
        this.code = code;
        this.balance = balance;
    }

    public String getCode() {
        return code;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(code, account.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public String toString() {
        return "Account{" +
                "code='" + code + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public int compareTo(Account o) {
        return code.compareTo(o.code);
    }
}
