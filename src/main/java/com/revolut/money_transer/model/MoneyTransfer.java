package com.revolut.money_transer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @author Igor
 */
public class MoneyTransfer {
    private String accountFrom;
    private String accountTo;
    private BigDecimal amount;

    @JsonCreator
    public MoneyTransfer(@JsonProperty(value = "accountFrom", required = true) String accountFrom,
                         @JsonProperty(value = "accountTo", required = true) String accountTo,
                         @JsonProperty(value = "amount", required = true) BigDecimal amount) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amount = amount;
    }

    public String getAccountFrom() {
        return accountFrom;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "MoneyTransfer{" +
                "accountFrom='" + accountFrom + '\'' +
                ", accountTo='" + accountTo + '\'' +
                ", amount=" + amount +
                '}';
    }
}
