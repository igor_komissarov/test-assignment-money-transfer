package com.revolut.money_transer.model;

/**
 * @author Igor
 */
public class AccountNotFoundException extends RuntimeException {
    public AccountNotFoundException(String accountCode) {
        super("Account not found: " + accountCode);
    }
}
