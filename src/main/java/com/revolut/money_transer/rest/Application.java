package com.revolut.money_transer.rest;

import com.revolut.money_transer.dao.AccountDao;
import com.revolut.money_transer.dao.AccountDaoImpl;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.inject.Singleton;

/**
 * @author Igor
 */
public class Application extends ResourceConfig {

    public Application() {
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(AccountDaoImpl.class).to(AccountDao.class).in(Singleton.class);
            }
        });
        register(JacksonFeature.class);
        packages("com.revolut.money_transer.rest", "org.codehaus.jackson.jaxrs");
    }
}
