package com.revolut.money_transer.rest;

import com.revolut.money_transer.dao.AccountDao;
import com.revolut.money_transer.model.Account;
import com.revolut.money_transer.model.MoneyTransfer;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * @author Igor
 */
@Path("/money-transfer")
public class MoneyTransferService {
    private final AccountDao accountDao;

    @Inject
    public MoneyTransferService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @GET
    @Path("/health-check")
    public String healthCheck() {
        return "Money transfer service is up and running";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void transfer(MoneyTransfer transfer) {

        if (transfer.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).entity("Transfer amount must be greater than zero").build());
        }
        if (transfer.getAccountFrom().equals(transfer.getAccountTo())) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).entity("Accounts for money transfer must be different").build());
        }

        Account accountFrom = accountDao.get(transfer.getAccountFrom());
        Account accountTo = accountDao.get(transfer.getAccountTo());

        //determine an order of the accounts to avoid deadlock
        Account account1, account2;
        if (accountFrom.compareTo(accountTo) < 0) {
            account1 = accountFrom;
            account2 = accountTo;
        } else {
            account1 = accountTo;
            account2 = accountFrom;
        }

        synchronized (account1) {
            synchronized (account2) {
                if (accountFrom.getBalance().compareTo(transfer.getAmount()) < 0) {
                    throw new WebApplicationException(Response.status(422)
                            .entity("Current balance " + accountFrom.getBalance() + " is less than transfer amount " +
                                    transfer.getAmount()).build());
                }
                accountFrom.setBalance(accountFrom.getBalance().subtract(transfer.getAmount()));
                accountTo.setBalance(accountTo.getBalance().add(transfer.getAmount()));
                accountDao.update(accountFrom);
                accountDao.update(accountTo);
            }
        }
    }
}
