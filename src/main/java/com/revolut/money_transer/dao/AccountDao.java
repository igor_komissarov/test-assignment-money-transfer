package com.revolut.money_transer.dao;

import com.revolut.money_transer.model.Account;

/**
 * @author Igor
 */
public interface AccountDao {

    Account get(String accountCode);

    void update(Account account);

    void create(Account account);

    void delete(Account account);
}
