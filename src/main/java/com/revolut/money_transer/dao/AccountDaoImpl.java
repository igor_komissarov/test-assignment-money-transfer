package com.revolut.money_transer.dao;

import com.revolut.money_transer.model.Account;
import com.revolut.money_transer.model.AccountNotFoundException;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Igor
 */
public class AccountDaoImpl implements AccountDao {
    private final Set<Account> store = new HashSet<>();

    {
        store.add(new Account("12345", BigDecimal.ZERO));
        store.add(new Account("12346", BigDecimal.ZERO));
        store.add(new Account("12347", BigDecimal.TEN));
        store.add(new Account("12348", BigDecimal.TEN));
        store.add(new Account("12349", BigDecimal.TEN));
    }

    @Override
    public Account get(String accountCode) {
        return store.stream().filter(account -> account.getCode().equals(accountCode)).findFirst()
                .orElseThrow(() -> new AccountNotFoundException(accountCode));
    }

    @Override
    public void update(Account account) {
        //nothing is required to do with such an implementation
    }

    @Override
    public void create(Account account) {
        store.add(account);
    }

    @Override
    public void delete(Account account) {
        store.remove(account);
    }
}
