package com.revolut.money_transer.rest;

import com.revolut.money_transer.dao.AccountDao;
import com.revolut.money_transer.model.Account;
import com.revolut.money_transer.model.AccountNotFoundException;
import com.revolut.money_transer.model.MoneyTransfer;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

/**
 * @author Igor
 */
public class MoneyTransferServiceTest {
    public static final String CODE_FULL = "12345";
    public static final String CODE_EMPTY = "12346";
    public static final String CODE_NOT_FOUND = "12347";
    public static final BigDecimal BALANCE_FULL = BigDecimal.TEN;
    public static final BigDecimal BALANCE_EMPTY = BigDecimal.ZERO;

    private Account accountFull;
    private Account accountEmpty;
    private AccountDao accountDao;
    private MoneyTransferService service;

    @Before
    public void setUp() throws Exception {
        accountFull = new Account(CODE_FULL, BALANCE_FULL);
        accountEmpty = new Account(CODE_EMPTY, BALANCE_EMPTY);
        accountDao = mock(AccountDao.class);
        when(accountDao.get(CODE_FULL)).thenReturn(accountFull);
        when(accountDao.get(CODE_EMPTY)).thenReturn(accountEmpty);
        when(accountDao.get(CODE_NOT_FOUND)).thenThrow(new AccountNotFoundException(""));
        service = new MoneyTransferService(accountDao);
    }

    @Test
    public void transferOk() throws Exception {
        service.transfer(new MoneyTransfer(CODE_FULL, CODE_EMPTY, new BigDecimal(5)));

        verify(accountDao).update(accountFull);
        verify(accountDao).update(accountEmpty);

        assertEquals(new BigDecimal(5), accountFull.getBalance());
        assertEquals(new BigDecimal(5), accountEmpty.getBalance());
    }

    @Test
    public void notEnoughMoney() throws Exception {
        try {
            service.transfer(new MoneyTransfer(CODE_EMPTY, CODE_FULL, new BigDecimal(5)));
            assertFalse(true);
        } catch (WebApplicationException e) {
            verify(accountDao, never()).update(any());

            assertEquals(BALANCE_FULL, accountFull.getBalance());
            assertEquals(BALANCE_EMPTY, accountEmpty.getBalance());
        }
    }

    @Test
    public void incorrectAmount() throws Exception {
        try {
            service.transfer(new MoneyTransfer(CODE_FULL, CODE_EMPTY, new BigDecimal(-5)));
            assertFalse(true);
        } catch (WebApplicationException e) {
            verify(accountDao, never()).update(any());

            assertEquals(BALANCE_FULL, accountFull.getBalance());
            assertEquals(BALANCE_EMPTY, accountEmpty.getBalance());
        }
    }

    @Test
    public void accountsTheSame() throws Exception {
        try {
            service.transfer(new MoneyTransfer(CODE_FULL, CODE_FULL, new BigDecimal(5)));
            assertFalse(true);
        } catch (WebApplicationException e) {
            verify(accountDao, never()).update(any());

            assertEquals(BALANCE_FULL, accountFull.getBalance());
            assertEquals(BALANCE_EMPTY, accountEmpty.getBalance());
        }
    }

    @Test
    public void accountNotFound() throws Exception {
        try {
            service.transfer(new MoneyTransfer(CODE_NOT_FOUND, CODE_FULL, new BigDecimal(5)));
            assertFalse(true);
        } catch (AccountNotFoundException e) {
            verify(accountDao, never()).update(any());

            assertEquals(BALANCE_FULL, accountFull.getBalance());
            assertEquals(BALANCE_EMPTY, accountEmpty.getBalance());
        }
    }
}